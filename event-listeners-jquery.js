
var height = '500px';

//listen to user events from extension
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log(sender.tab ?
            "Content-Script : Got Message (" + JSON.stringify(request.message) + ") from a content script:" + sender.tab.url :
            "Content-Script : Got Message (" + JSON.stringify(request.message) + ") from the extension");
    if (request.message === "SHOW_TOOLBAR"){

      localStorage.setItem("mysample123123toolbar-url", window.location.href);
      showToolbar();
      sendResponse({message: "TOOLBAR_ON"});
    }
    else if (request.message === "REMOVE_TOOLBAR"){
      removeToolbar();
      sendResponse({message: "TOOLBAR_OFF"});
    }

});

function showToolbar(){
  console.log("going to prepend Toolbar")
  var toolbarURL = chrome.extension.getURL("toolbar/toolbar.html")
  console.log("toolbar url  - " + toolbarURL);

  $('<iframe />');  // Create an iframe element
            $('<iframe />', {
                name: 'frame1',
                id: 'mysample123123toolbar',
                src: toolbarURL ,
                sandbox: "allow-same-origin allow-scripts",
                style: 'height:' + height
            }).appendTo('html');

  $('body').css({
    '-webkit-transform': 'translateY(' + height+ ')'
  })

  var iframeWin = document.getElementById("mysample123123toolbar").contentWindow;
  iframeWin.postMessage(document.URL, '*');
  console.log("Sent message to iframe")
  console.log("show toolbar done..")
}

window.onmessage = function(e){
    if (e.data == 'hello') {
        alert('It works!');
    }
};

function removeToolbar(){

  $('#mysample123123toolbar').remove();
  $('body').css({
    '-webkit-transform': 'translateY(-1px)'
  })

}
