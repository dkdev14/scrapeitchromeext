//record all steps performed
var steps = null;
var stepNo = 0;
var guid = null;
var stepText = null;
var state = null;
var someStorageKey = 'f4vs-vfg5-ymu46-6g5t-43453';
var isOn = false;

chrome.browserAction.onClicked.addListener(function (tab) {
  if(!isOn){
    sendMessageToScript({message: "SHOW_TOOLBAR"});
  }
  else {
    sendMessageToScript({message: "REMOVE_TOOLBAR"});
  }
});

function sendMessageToScript(msg){
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    console.log(tabs[0].id);
    chrome.tabs.sendMessage(tabs[0].id, msg, function(response){
      if(response && response.message){
        if(response.message === "TOOLBAR_ON"){
          isOn = true;
        }
        else if(response.message === "TOOLBAR_OFF"){
          isOn = false;
        }
      }
    });
 });
}
