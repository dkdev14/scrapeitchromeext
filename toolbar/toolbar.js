$(document).ready(function(){

  var maxDataKeysAllowed = 15;
  //append only in iframe
  if(parent === top){
    window.top.postMessage("hello", '*');
    console.log("Loaded..")
    var url;
    window.onmessage = function(e){
        console.log("IFRAME workls...........")
        alert('Iframe works - ' + e.data);
        $("baseUrl").val(e.data);
    };

    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    $("#baseUrl").change(function(){
      console.log("Text changed ")
      url = $(this).val();
      $("#loopURL").val(url)
      console.log("Loop url - " + $(this).val())
      console.log("Current Page No - " + getCurrentPageNo(url))
      $("#currentPageNo").val("Current Page No. : " + getCurrentPageNo(url))
    })

    $(".next").click(function(){

    	if(animating) return false;
    	animating = true;

    	current_fs = $(this).parent();
    	next_fs = $(this).parent().next();

    	//activate next step on progressbar using the index of next_fs
    	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    	//show the next fieldset
    	next_fs.show();
    	//hide the current fieldset with style
    	current_fs.animate({opacity: 0}, {
    		step: function(now, mx) {
    			//as the opacity of current_fs reduces to 0 - stored in "now"
    			//1. scale current_fs down to 80%
    			scale = 1 - (1 - now) * 0.2;
    			//2. bring next_fs from the right(50%)
    			left = (now * 50)+"%";
    			//3. increase opacity of next_fs to 1 as it moves in
    			opacity = 1 - now;
    			current_fs.css({
            'transform': 'scale('+scale+')',
            'position': 'absolute'
          });
    			next_fs.css({'left': left, 'opacity': opacity});
    		},
    		duration: 800,
    		complete: function(){
    			current_fs.hide();
    			animating = false;
    		},
    		//this comes from the custom easing plugin
    		easing: 'easeInOutBack'
    	});
    });

    $(".previous").click(function(){
    	if(animating) return false;
    	animating = true;

    	current_fs = $(this).parent();
    	previous_fs = $(this).parent().prev();

    	//de-activate current step on progressbar
    	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    	//show the previous fieldset
    	previous_fs.show();
    	//hide the current fieldset with style
    	current_fs.animate({opacity: 0}, {
    		step: function(now, mx) {
    			//as the opacity of current_fs reduces to 0 - stored in "now"
    			//1. scale previous_fs from 80% to 100%
    			scale = 0.8 + (1 - now) * 0.2;
    			//2. take current_fs to the right(50%) - from 0%
    			left = ((1-now) * 50)+"%";
    			//3. increase opacity of previous_fs to 1 as it moves in
    			opacity = 1 - now;
    			current_fs.css({'left': left});
    			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
    		},
    		duration: 800,
    		complete: function(){
    			current_fs.hide();
    			animating = false;
    		},
    		//this comes from the custom easing plugin
    		easing: 'easeInOutBack'
    	});
    });

    $(".submit").click(function(){
    	return false;
    })

    /* Variables */
    var p = $("#participants").val();
    var row = $(".dataRow");

    /* Functions */
    function getP(){
      p = $("#participants").val();
    }

    function addRow() {
      row.clone(true, true).appendTo("#dataTable");
    }

    function removeRow(button) {
      button.closest("tr").remove();
    }
    /* Doc ready */
    $(".add").on('click', function () {
      getP();
      if($("#dataTable tr").length < maxDataKeysAllowed) {
        addRow();
        var i = Number(p)+1;
        $("#participants").val(i);
      }
      $(this).closest("tr").appendTo("#dataTable");
    });

    $(".remove").on('click', function () {
      getP();
      if($("#dataTable tr").length === 3) {
        //alert("Can't remove row.");
        $(".remove").hide();
      } else if($("#dataTable tr").length - 1 ==3) {
        $(".remove").hide();
        removeRow($(this));
        var i = Number(p)-1;
        $("#participants").val(i);
      } else {
        removeRow($(this));
        var i = Number(p)-1;
        $("#participants").val(i);
      }
    });
    $("#participants").change(function () {
      var i = 0;
      p = $("#participants").val();
      var rowCount = $("#dataTable tr").length - 2;
      if(p > rowCount) {
        for(i=rowCount; i<p; i+=1){
          addRow();
        }
        $("#dataTable #addButtonRow").appendTo("#dataTable");
      } else if(p < rowCount) {
      }
    });
  }

  //get query params
  function getCurrentPageNo(url, lastPage){
    console.log("Handle URL - " + url)
    var index = url.indexOf("page=");
    if(index !== -1){
      var secondPart = url.substr(index + 5, url.length - 1);
      var pageNumEndIndex = secondPart.indexOf('&');
      var currentPage = url.substr(index + 5, pageNumEndIndex);
      return currentPage;
    }else {
      return "Not Found";
    }

  }

});
